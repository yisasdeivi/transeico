<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->middleware('guest');
Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::get('cargarExcel','formcontroller@formexcel');
Route::get('listado_datos/{page?}', 'listar_controlador@listar_datospaginacion');
Route::get('listado_cliente/{page?}', 'listar_controlador@listar_cliente');
Route::get('listado_Admin', 'listar_controlador@listar_Admin');
Route::get('listado_log', 'listar_controlador@listar_log');
Route::get('buscar_cliente/{page?}/{buscar?}', 'listar_controlador@buscarclientes');
Route::get('buscar_datos/{page?}/{buscar?}', 'listar_controlador@buscardatos');
Route::get('docvencido', 'vencido_controlador@index');
Route::get('docvencidos/{in?}/{end?}', 'vencido_controlador@documentoVencido');
Route::get('dashboard', 'dashboard_controlador@consolidadoChart');
Route::get('eliminarAdmin/{email?}','Auth\RegisterController@delete');
Route::get('eliminarlogid/{id?}','listar_controlador@eliminarlogid');
Route::get('eliminarlogtodo','listar_controlador@eliminarlogtodo');

Route::get('inicio', function () {
    return view('index');
})->middleware('auth');
Route::get ('registro', function () {
    return view('registrar');
})->middleware('auth');
Route::get('password',function(){
	return view('auth.passwords.reset');
});
Route::post('login','Auth\LoginController@login')->name('login');
Route::post('cargar_datos','datos_controlador@cargardatos');
Route::post('registrar','Auth\RegisterController@create');
Auth::routes();

