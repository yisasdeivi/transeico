<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('datosvehiculos/{cedula?}','api_controlador@listarVencidos')->middleware('cors');
Route::get('vehiculos/{cedula?}','api_controlador@listarplaca')->middleware('cors');
Route::get('buscarxplaca/{placa?}','api_controlador@buscarxplaca')->middleware('cors');
Route::get('buscarxplacamesvencido{placa?}','api_controlador@buscarxplacamesvencido')->middleware('cors');
