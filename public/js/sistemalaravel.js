function eliminartodo(){       
     var url = "eliminarlogtodo";
     var i=confirm("Esta seguro de eliminar todo estos registros?");
     var div="<div class='alert alert-danger' role='alert'>";
     if(i==true){     
        $("#contenido_principal").html($("#cargador_empresa").html());       
             $.get(url,function(resul){
                $("#contenido_principal").html(div+resul+"</div>");
            })


      }
}
//elimina un log por id
function eliminarlogid(id){
      var url = "eliminarlogid"+"?"+"id="+id;
         
       $("#contenido_principal").html($("#cargador_empresa").html());

    
    $.get(url,function(resul){

        $("#contenido_principal").html(resul); 
   })


}

function cargarSubirArchivo(){
    //funcion que carga todos los formularios del sistema

   var url = "cargarExcel";    

    $("#contenido_principal").html($("#cargador_empresa").html());       
    $.get(url,function(resul){
      $("#contenido_principal").html(resul);
    })
        

}

function registrarAdmin(){
    //funcion que carga todos los formularios del sistema

   var url = "register";    

    $("#contenido_principal").html($("#cargador_empresa").html());       
    $.get(url,function(resul){
      $("#contenido_principal").html(resul);
    })
        

}


function dashboard(){
  var url="dashboard";
  
  $("#contenido_principal").html($("#cargador_empresa").html());

    
    $.get(url,function(resul){

        $("#contenido_principal").html(resul); 
   })

}
//elimina Administrador 
function eliminarAdmin(email){       
     var url = "eliminarAdmin"+"?"+"email="+email;
     var i=confirm("Esta seguro de eliminar este Administrador?");
     var div="<div class='alert alert-danger' role='alert'>";
     if(i==true){     
        $("#contenido_principal").html($("#cargador_empresa").html());       
             $.get(url,function(resul){
                $("#contenido_principal").html(div+resul+"</div>");
            })


      }
}

function cargarlistado(listado){

    //funcion para cargar los diferentes  en general

if(listado==1){ var url = "listado_datos"; }
if(listado==2){ var url = "listado_cliente"; }
if(listado==3){ var url = "docvencido"; }
if(listado==4){var url="listado_Admin";}
if(listado==5){var url="listado_log";}
$("#contenido_principal").html($("#cargador_empresa").html());

    
    $.get(url,function(resul){

        $("#contenido_principal").html(resul); 
   })



}

$(document).on("submit","registrar",function(){       
        var url = "registrar";
        $.ajax({                        
           type: "POST",                 
           url: url,                     
           data: $("#registrar").serialize(), 
           success: function(data)             
           {
             $('#resp').html(data);               
           }
       });
});


 $(document).on("submit",".form_venci",function(e){

//funcion para atrapar los formularios y enviar los datos

       e.preventDefault();
        var start=$('#in').val();
        var end=$('#end').val();
        var url="docvencidos"+"?in="+start+"&end="+end; 
        $("#contenido_principal").html($("#cargador_empresa").html());
          $.get(url,function(resul){            
            $("#contenido_principal").html(resul); 
 })


})

var x=false;
var aux='';

$(document).on("submit",".form_buscarU",function(e){

//funcion para atrapar los formularios y enviar los datos

       e.preventDefault();
        var buscar=$(this).serialize();
        var url="buscar_cliente"+"?"+buscar; 
        aux=buscar;
        $("#contenido_principal").html($("#cargador_empresa").html());
          $.get(url,function(resul){            
            $("#contenido_principal").html(resul); 
 })


})

 $(document).on("submit",".form_buscar",function(e){

//funcion para atrapar los formularios y enviar los datos

       e.preventDefault();
        var buscar=$(this).serialize();
        var url="buscar_datos"+"?"+buscar; 
        aux=buscar;
        $("#contenido_principal").html($("#cargador_empresa").html());
          $.get(url,function(resul){            
            $("#contenido_principal").html(resul); 
 })


})


$(document).on("click",".pagination li a",function(e){
//para que la pagina se cargen los elementos
 e.preventDefault();
 if(!aux===''){
   var url =$( this).attr("href");
 }else{
   var url =$( this).attr("href")+'&'+aux;
   
 }
 $("#contenido_principal").html($("#cargador_empresa").html());

 $.get(url,function(resul){
    $("#contenido_principal").html(resul); 
 })

})





$(document).on("click",".div_modal",function(e){
 //funcion para ocultar las capas modales
 $("#capa_modal").hide();
 $("#capa_para_edicion").hide();
 $("#capa_para_edicion").html("");

})  





  //subir archivo Excel


  $(document).on("submit",".formarchivo",function(e){

     
        e.preventDefault();
        var formu=$(this);
        var nombreform=$(this).attr("id");
     
        var miurl="cargar_datos";  
        var divresul="notificacion_resul_fcdu";

        //información del formulario
        var formData = new FormData($("#"+nombreform+"")[0]);

        //hacemos la petición ajax   
        $.ajax({
            url: miurl,  
            type: 'POST',
     
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            //mientras enviamos el archivo
            beforeSend: function(){
              $("#"+divresul+"").html($("#cargador_empresa").html());                
            },
            //una vez finalizado correctamente
            success: function(data){
              $("#"+divresul+"").html(data);
              $("#fotografia_usuario").attr('src', $("#fotografia_usuario").attr('src') + '?' + Math.random() );               
            },
            //si ha ocurrido un error
            error: function(data){
               alert("ha ocurrido un error") ;
                
            }
        });
    });