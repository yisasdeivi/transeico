<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use Carbon\Carbon;

class enviarcorreo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar-correo:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Por tarea programada se le envia correo a los usuarios que tengan los documentos obligatorios vencido';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {  
        
       $info="";
        $mes = date('m');
        $year= date('Y');
        $url="http://notificacion.transeicosas.com/imagenes/alerta.gif";
        $url2="http://notificacion.transeicosas.com/imagenes/vencimiento.png";
        $urlv="http://notificacion.transeicosas.com/imagenes/emergencia.gif";
        $urlv2="http://notificacion.transeicosas.com/imagenes/vencidos.png";

        $dato=DB::select( DB::raw('select * from (select placa,marca,modelo,cedula_propietario,(CASE WHEN fecha_soat BETWEEN \''.$year.'-'.$mes.'-01 \'AND \''.$year.'-'.$mes.'-30\'  THEN fecha_soat END)as soat ,
       (CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_poliza_resp  END) as polizaresp ,
       (CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_tarjetaop END) as tarjetaop ,
       (CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_todo_riesgo  END) as todo ,
       (CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_tecnomecanica  END) as tecno , 
       (CASE WHEN fecha_preventiva BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_preventiva END)  as preventiva from vehiculo)as consulta inner join persona p on cedula_propietario = p.cedula where not (soat is null and polizaresp is null and tarjetaop is null and todo is null and tecno is null and preventiva is null)
      '));
        $htmlE="<p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:40px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>&nbsp;<b>";

        ///cuerpo alerta
        $htmlcuerp="</b></p>
      <table class='callout' style='Margin-bottom:16px;border-collapse:collapse;border-spacing:0;margin-bottom:16px;padding:0;text-align:left;vertical-align:top;width:100%'>
      <tbody>
        <tr>
         <th class='callout-inner' style='Margin:0;background:#FFB778;border:1px solid #cbcbcb;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:10px;text-align:left;width:100%'><p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>";

         //html fecha inicio
         $htmlfecha=":</p><h6 style='Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal'><b>";

         //htmls fecha final
         $htmlfechafinal="</b></h6><p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'> Placa del Vehiculo:<b>";
         
       ///cuerpo warning
       $htmlcuerpW="</b></p>
      <table class='callout' style='Margin-bottom:16px;border-collapse:collapse;border-spacing:0;margin-bottom:16px;padding:0;text-align:left;vertical-align:top;width:100%'>
      <tbody>
        <tr>
         <th class='callout-inner' style='Margin:0;background:#FFB778;border:1px solid #cbcbcb;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:10px;text-align:left;width:100%'><p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'><b>";
    

                    $hmtlEnd="</b></p></th>

         <th class='expander' style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0'>
        </table>";
    $cuerplog="";
    $cuerpo="";
        foreach ($dato as $key => $dat) {
            $cont=0;
            $soat=$dat->soat;
            $polizaresp=$dat->polizaresp;
            $tarjetaop=$dat->tarjetaop;
            $todo=$dat->todo;
            $tecno=$dat->tecno;
            $preventiva=$dat->preventiva;
            $email=$dat->email;
            // validacion si los documentos estan vencidos SOAT
        if(($soat) !=null && ($email)!=null){
            if(($soat) <= Carbon::now()){
                $cuerp="";
                $cuerp.=$htmlE."Soat".$htmlcuerpW;               
                  $fecha = Carbon::parse($soat);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  if($diasDi==0)  $cuerp.="El documento vence hoy ".$htmlfecha.$soat.$htmlfechafinal;
                  else $cuerp.="El documento venció el ".$htmlfecha.$soat.$htmlfechafinal;
                  $cuerp.=$dat->placa.$hmtlEnd;
                  $this->info('alerta vencido por '.$diasDi.'');  
                  $info.="correo por "."Soat"." caducado:"." "."<b>".$dat->email."</b>"."  Placa del vehiculo".$dat->placa."<br>";
                  $data = [
                     'color'  => '#d13402',
                     'nombre' => $dat->nombre,
                     'correo' => $dat->email,
                     'cuerpo' => $cuerp,
                     'url'    => $urlv,
                     'url2'   => $urlv2,
                     'marca'  => $dat->marca,
                     'modelo' => $dat->modelo,
                     'placa'  => $dat->placa,
                   ];
         Mail::send('correos.correo', ['data' => $data], function ($message) use ($data) {

         $message->from('notificacion@transeicosas.com', 'Notificacion');

        $message->to($data['correo'],$data['nombre'])->subject('correo de notificacion Transeico')->cc('notificacion@transeicosas.com');

               });
     }
}
      // validacion si los documentos estan vencidos Poliza Responsabilidad
        if(($polizaresp) !=null && ($email)!=null){
            if(($polizaresp) <= Carbon::now()){
            	
                $cuerp="";
                $cuerp.=$htmlE."Poliza RCC Y RCE".$htmlcuerp;
                  $fecha = Carbon::parse($polizaresp);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                   if($diasDi==0)  $cuerp.="El documento vence hoy ".$htmlfecha.$polizaresp.$htmlfechafinal;
                  else $cuerp.="El documento venció el ".$htmlfecha.$polizaresp.$htmlfechafinal;
                  $cuerp.=$dat->placa.$hmtlEnd;
                  $this->info('alerta vencido por '.$diasDi.'');   
                    $info.="correo por "."Poliza RCC Y RCE"." caducado"." "."<b>".$dat->email."</b>"." Placa del vehiculo".$dat->placa."<br>";
                  $data = [
                     'color'  => '#d13402',
                     'nombre' => $dat->nombre,
                     'correo' => $dat->email,
                     'cuerpo' => $cuerp,
                     'url'    => $urlv,
                     'url2'   => $urlv2,
                    'marca'  => $dat->marca,
                     'modelo' => $dat->modelo,
                     'placa'  => $dat->placa,
                   ];
         Mail::send('correos.correo', ['data' => $data], function ($message) use ($data) {

         $message->from('notificacion@transeicosas.com', 'Notificacion');

        $message->to($data['correo'],$data['nombre'])->subject('correo de notificacion Transeico')->cc('notificacion@transeicosas.com');

               });
     }
}

    // validacion si los documentos estan vencidos TarjetaOperacion
        if(($tarjetaop) !=null && ($email)!=null){
            if(($tarjetaop) <= Carbon::now()){
                $cuerp="";
                $cuerp.=$htmlE."Tarjeta de Operación".$htmlcuerp;
                  $fecha = Carbon::parse($tarjetaop);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  if($diasDi==0)  $cuerp.="El documento vence hoy ".$htmlfecha.$tarjetaop.$htmlfechafinal;
                  else $cuerp.="El documento venció el ".$htmlfecha.$tarjetaop.$htmlfechafinal;
                    $cuerp.=$dat->placa.$hmtlEnd;
                  $this->info('alerta vencido por '.$diasDi.'');  
                    $info.="correo por "."Tarjeta de Operación"." caducado:"." "."<b>".$dat->email."</b>"." Placa del vehiculo".$dat->placa."<br>";
                  $data = [
                     'color'  => '#d13402',
                     'nombre' => $dat->nombre,
                     'correo' => $dat->email,
                     'cuerpo' => $cuerp,
                     'url'    => $urlv,
                     'url2'   => $urlv2,
                    'marca'  => $dat->marca,
                     'modelo' => $dat->modelo,
                     'placa'  => $dat->placa,
                   ];
         Mail::send('correos.correo', ['data' => $data], function ($message) use ($data) {

         $message->from('notificacion@transeicosas.com', 'Notificacion');

        $message->to($data['correo'],$data['nombre'])->subject('correo de notificacion Transeico')->cc('notificacion@transeicosas.com');

               });
     }
}

 // validacion si los documentos estan vencidos Todo Riesgo
        if(($todo) !=null && ($email)!=null){
            if(($todo) <= Carbon::now()){
                $cuerp="";

                $cuerp.=$htmlE."Seguro Todo Riesgo".$htmlcuerp;
                  $fecha = Carbon::parse($todo);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  if($diasDi==0)  $cuerp.="El documento vence hoy ".$htmlfecha.$todo.$htmlfechafinal;
                  else $cuerp.="El documento venció el ".$htmlfecha.$todo.$htmlfechafinal;
                    $cuerp.=$dat->placa.$hmtlEnd; 
                    $info.="correo por "."Seguro Todo Riesgo"." caducado:"." "."<b>".$dat->email."</b>"." Placa del vehiculo".$dat->placa."<br>";
                  $data = [
                     'color'  => '#d13402',
                     'nombre' => $dat->nombre,
                     'correo' => $dat->email,
                     'cuerpo' => $cuerp,
                     'url'    => $urlv,
                     'url2'   => $urlv2,
                     'marca'  => $dat->marca,
                     'modelo' => $dat->modelo,
                     'placa'  => $dat->placa,
                   ];
         Mail::send('correos.correo', ['data' => $data], function ($message) use ($data) {

         $message->from('notificacion@transeicosas.com', 'Notificacion');

        $message->to($data['correo'],$data['nombre'])->subject('correo de notificacion Transeico')->cc('notificacion@transeicosas.com');

               });
     }
}


 // validacion si los documentos estan vencidos Tecnomecanica
        if(($tecno) !=null && ($email)!=null){
            if(($tecno) <= Carbon::now()){
                $cuerp="";
                $cuerp.=$htmlE."Revisión Tecnomecanica".$htmlcuerp;
                  $fecha = Carbon::parse($tecno);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  if($diasDi==0)  $cuerp.="El documento vence hoy ".$htmlfecha.$tecno.$htmlfechafinal;
                  else $cuerp.="El documento venció el ".$htmlfecha.$tecno.$htmlfechafinal;
                  $cuerp.=$dat->placa.$hmtlEnd; 
                  $this->info('alerta vencido por '.$diasDi.'');  
                    $info.="correo por "."Revisión Tecnomecanica"." caducado:"." "."<b>".$dat->email."</b>"." Placa del vehiculo".$dat->placa."<br>";
                  $data = [
                     'color'  => '#d13402',
                     'nombre' => $dat->nombre,
                     'correo' => $dat->email,
                     'cuerpo' => $cuerp,
                     'url'    => $urlv,
                     'url2'   => $urlv2,
                     'marca'  => $dat->marca,
                     'modelo' => $dat->modelo,
                     'placa'  => $dat->placa,
                   ];
         Mail::send('correos.correo', ['data' => $data], function ($message) use ($data) {

         $message->from('notificacion@transeicosas.com', 'Notificacion');

        $message->to($data['correo'],$data['nombre'])->subject('correo de notificacion Transeico')->cc('notificacion@transeicosas.com');

               });
     }
}

// validacion si los documentos estan vencidos Preventiva
        if(($preventiva) !=null && ($email)!=null){
            if(($preventiva) <= Carbon::now()){
                $cuerp="";
                $cuerp.=$htmlE."Preventiva".$htmlcuerp;
                  $fecha = Carbon::parse($preventiva);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  if($diasDi==0)  $cuerp.="El documento vence hoy ".$htmlfecha.$preventiva.$htmlfechafinal;
                  else $cuerp.="El documento venció el ".$htmlfecha.$preventiva.$htmlfechafinal;
                  $cuerp.=$dat->placa.$hmtlEnd; 
                  $this->info('alerta vencido por '.$diasDi.'');  
                   $info.="correo por "."Preventiva"." caducado:"." "."<b>".$dat->email."</b>"." Placa del vehiculo".$dat->placa."<br>";
                  $data = [
                     'color'  => '#d13402',
                     'nombre' => $dat->nombre,
                     'correo' => $dat->email,
                     'cuerpo' => $cuerp,
                     'url'    => $urlv,
                     'url2'   => $urlv2,
                     'marca'  => $dat->marca,
                     'modelo' => $dat->modelo,
                     'placa'  => $dat->placa,
                   ];
         Mail::send('correos.correo', ['data' => $data], function ($message) use ($data) {

         $message->from('notificacion@transeicosas.com', 'Notificacion');

        $message->to($data['correo'],$data['nombre'])->subject('correo de notificacion Transeico')->cc('notificacion@transeicosas.com');

               });
     }
}
/****************************************************************************************
 ****************************DOCUMENTOS A VENCER ****************************************
 ****************************************************************************************
**/

      if(($email)!=null){
            //documento sooat ,validacion de enviar correo
            if(($soat) !=null){                
                if(($soat) > Carbon::now()){
                  $cuerpo.=$htmlE."Soat".$htmlcuerp;  
                  $cuerplog.=", <b>Soat:</b>"." "."<b>".$soat."</b>";
                  $fecha = Carbon::parse($soat);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  $cuerpo.="El documento vence el: ".$htmlfecha.$soat.$htmlfechafinal;
                  $cont=$cont+1;
                  $cuerpo.=$dat->placa.$hmtlEnd;

                }
            }
        
           
    //documento poliza de responsabilidad ,validacion de enviar 
        
         if(($polizaresp) !=null){               
                if(($polizaresp) > Carbon::now()){
                  $cuerpo.=$htmlE."Poliza RCC Y RCE".$htmlcuerp;
                   $cuerplog.=" <b>Poliza RCC Y RCE:</b>"." "."<b>".$polizaresp."</b>,";
                  $fecha = Carbon::parse($polizaresp);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  $cuerpo.="El documento vence el: ".$htmlfecha.$polizaresp.$htmlfechafinal;
                  $cont=$cont+1;
                  $cuerpo.=$dat->placa.$hmtlEnd;
                }
            }
        
    //documento Tarjet de operacion ,validacion de enviar 
      
         if(($tarjetaop) !=null){                
                if(($tarjetaop) > Carbon::now()){
                  $cuerpo.=$htmlE."Tarjeta de Operación".$htmlcuerp;
                  $cuerplog.=" <b>Tarjeta de Operación:</b>"." "."<b>".$tarjetaop."</b>,";
                  $fecha = Carbon::parse($tarjetaop);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  $cuerpo.="El documento vence el: ".$htmlfecha.$tarjetaop.$htmlfechafinal;
                  $cont=$cont+1;
                  $cuerpo.=$dat->placa.$hmtlEnd;
                }
            }
        
        //documento Todo Riesgo ,validacion de enviar 
       
         if(($todo) !=null){                
                if(($todo) > Carbon::now()){
                  $cuerpo.=$htmlE."Seguro Todo Riesgo".$htmlcuerp;
                  $cuerplog.="<b> Seguro Todo Riesgo:</b>"." "."<b>".$todo."</b>,";
                  $fecha = Carbon::parse($todo);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  $cuerpo.="El documento vence el: ".$htmlfecha.$todo.$htmlfechafinal;
                  $cont=$cont+1;
                  $cuerpo.=$dat->placa.$hmtlEnd;
                }
            }
        

   //documento Tecnomecanica ,validacion de enviar 
       
         if(($tecno) !=null){                
                if(($tecno) > Carbon::now()){
                  $cuerpo.=$htmlE."Revisión Tecnomecanica".$htmlcuerp;
                  $cuerplog.=" <b>Revisión Tecnomecanica:</b>"." "."<b>".$tecno."</b>,";
                  $fecha = Carbon::parse($tecno);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                $cuerpo.="El documento vence el: ".$htmlfecha.$tecno.$htmlfechafinal;
                  $cont=$cont+1;
                  $cuerpo.=$dat->placa.$hmtlEnd;
                }
            }
        

        //documento  preventiva,validacion de enviar 
    if(($preventiva) !=null){                 
                if(($preventiva) > Carbon::now()){
                  $cuerpo.=$htmlE."Revisión Preventiva".$htmlcuerp;
                  $cuerplog.=" <b>Revisión Preventiva</b>:"." "."<b>".$preventiva."</b>,";
                  $fecha = Carbon::parse($preventiva);
                  $fecha2= Carbon::parse(date('Y-m-d'));
                  $diasDi = $fecha->diffInDays($fecha2);
                  $cuerpo.="El documento vence el: ".$htmlfecha.$preventiva.$htmlfechafinal;
                  $cont=$cont+1;
                  $cuerpo.=$dat->placa.$hmtlEnd;
                }
            }
        
        
        if($cont>=1){
           $info.="correo por alerta de la Placa:<b>".$dat->placa."</b> Correo:<b>".$dat->email."</b><br>Documentos a vencer:(".$cuerplog.")<br><br>";
           $cuerplog="";
              $data = [
                     'color'  => '#FF6433',
                     'nombre' => $dat->nombre,
                     'correo' => $dat->email,
                     'cuerpo' => $cuerpo,
                     'url'    => $url,
                     'url2'   => $url2,
                    'marca'  => $dat->marca,
                     'modelo' => $dat->modelo,
                     'placa'  => $dat->placa,
                   ];
         Mail::send('correos.correo', ['data' => $data], function ($message) use ($data) {

         $message->from('notificacion@transeicosas.com', 'Notificacion');

        $message->to($data['correo'],$data['nombre'])->subject('correo de notificacion Transeico')->cc('notificacion@transeicosas.com');

               });
        $cuerpo="";
      
          }
        }
      }
        
        $infos[]=['descrip'=>'se envio a los siguientes correos:<br>'."\n".$info.'',
        'fecha' =>$date = Carbon::now()];
        $log=DB::table('log')->insert($infos);
        $this->info('Done');
    
    }
}
    