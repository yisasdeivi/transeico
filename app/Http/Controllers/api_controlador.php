<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class api_controlador extends Controller
{
    //
 public function __construct()
    {
        
    }


    public function listarVencidos(Request $req){
    	$mes = date('m');
        $year= date('Y');
        $cedula=$req->input("cedula");
        $dato=DB::select( DB::raw('select * from (select placa,marca,modelo,cedula_propietario,(CASE WHEN fecha_soat BETWEEN \''.$year.'-'.$mes.'-01 \'AND \''.$year.'-'.$mes.'-30\'  THEN fecha_soat END)as soat ,
       (CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_poliza_resp  END) as polizaresp ,
       (CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_tarjetaop END) as tarjetaop ,
       (CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_todo_riesgo  END) as todo ,
       (CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_tecnomecanica  END) as tecno , 
       (CASE WHEN fecha_preventiva BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_preventiva END)  as preventiva from vehiculo where cedula_propietario = '.$cedula.')as consulta inner join persona p on cedula_propietario = p.cedula where not (soat is null and polizaresp is null and tarjetaop is null and todo is null and tecno is null and preventiva is null)
      '));

        return response()->json($dato);

    }

    public function listarplaca(Request $req){
       $cedula=$req->input("cedula");
       $dato=DB::select( DB::raw('select placa from vehiculo where cedula_propietario='.$cedula.''));
        return response()->json($dato);

    }

     public function buscarxplaca(Request $req){
        $mes = date('m');
        $year= date('Y');
        $placa=$req->input("placa");
        $dato=DB::select( DB::raw('select * from (select placa,marca,modelo,cedula_propietario,(CASE WHEN fecha_soat BETWEEN \''.$year.'-'.'01-01 \'AND \''.$year.'12-31\'  THEN fecha_soat END)as soat ,
       (CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-'.'01-01 \'AND \''.$year.'-12-31\'  THEN fecha_poliza_resp  END) as polizaresp ,
       (CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-'.'01-01 \'AND \''.$year.'-12-31\'  THEN fecha_tarjetaop END) as tarjetaop ,
       (CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-'.'01-01 \'AND \''.$year.'-12-31\'  THEN fecha_todo_riesgo  END) as todo ,
       (CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-'.'01-01 \'AND \''.$year.'-12-31\'  THEN fecha_tecnomecanica  END) as tecno , 
       (CASE WHEN fecha_preventiva BETWEEN \''.$year.'-'.'01-01 \'AND \''.$year.'-12-31\'  THEN fecha_preventiva END)  as preventiva from vehiculo where placa= \''.$placa.'\')as consulta inner join persona p on cedula_propietario = p.cedula where not (soat is null and polizaresp is null and tarjetaop is null and todo is null and tecno is null and preventiva is null)
      '));

        return response()->json($dato);

    }
    public function buscarxplacamesvencido(Request $req){
      $mes = date('m');
        $year= date('Y');
        $placa=$req->input("placa");
        $dato=DB::select( DB::raw('select * from (select placa,marca,modelo,cedula_propietario,(CASE WHEN fecha_soat BETWEEN \''.$year.'-'.$mes.'-01 \'AND \''.$year.'-'.$mes.'-30\'  THEN fecha_soat END)as soat ,
       (CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_poliza_resp  END) as polizaresp ,
       (CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_tarjetaop END) as tarjetaop ,
       (CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_todo_riesgo  END) as todo ,
       (CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_tecnomecanica  END) as tecno , 
       (CASE WHEN fecha_preventiva BETWEEN \''.$year.'-'.$mes.'-01\' AND \''.$year.'-'.$mes.'-30\'  THEN fecha_preventiva END)  as preventiva from vehiculo where placa= \''.$placa.'\')as consulta inner join persona p on cedula_propietario = p.cedula where not (soat is null and polizaresp is null and tarjetaop is null and todo is null and tecno is null and preventiva is null)
      '));

        return response()->json($dato);

    }

   
}
