<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class listar_controlador extends Controller
{
    

     public function __construct()
    {
        $this->middleware('auth');
    }
    //presenta el formulario para nuevo usuario
		public function listar_datospaginacion()
   {
   	      
          $dato = DB::table('vehiculo')
                       ->join('persona','persona.cedula','=','vehiculo.cedula_propietario')->paginate(5);
         // $dato= DB::select( DB::raw('select * from vehiculo v INNER JOIN persona P ON P.cedula = v.cedula_propietario'));

          return view('listado.listadodatos', ['dato' => $dato]); 
    }

    public function buscardatos(Request $req)
    { 
    	$buscar=$req->input("buscar");
    	$dato=DB::table('vehiculo')->join('persona','persona.cedula','=','vehiculo.cedula_propietario')->where('placa','LIKE','%'.$buscar.'%')
    	                             ->orwhere('modelo','LIKE','%'.$buscar.'%')
                                   ->orwhere('cedula_propietario','LIKE','%'.$buscar.'%')
                                   ->orwhere('nombre','LIKE','%'.$buscar.'%')
    	                             ->paginate(5);
        
        return view('listado.listadodatos', ['dato' => $dato]); 
    }
    public function buscarclientes(Request $req){
      $buscar=$req->input("buscar");
      $dato=DB::table('persona')->where('nombre','LIKE','%'.$buscar.'%')
                                   ->orwhere('cedula','LIKE','%'.$buscar.'%')
                                   ->orwhere('email','LIKE','%'.$buscar.'%')
                                   ->paginate(5);
        
        return view('listado.listadocliente', ['dato' => $dato]); 
    }

    public function listar_cliente()
   {
          
          $dato = DB::table('persona')->paginate(20);

          return view('listado.listadocliente', ['dato' => $dato]); 
    }

    public function listar_Admin(){
      $dato=DB::table('users')->get();
      return view('listado.listarAdmin',['dato'=>$dato]);
    }

    public function listar_log()
    {
      $dato=DB::table('log')->paginate(1);
      return view('listado.log',['dato'=>$dato]);
    }

    public function eliminarlogid(Request $req)
    {
      $id=$req->input("id");
      $dato=DB::table('log')->delete($id);
      $dato=DB::table('log')->paginate(1);
      return view('listado.log',['dato'=>$dato]);
    }

    public function eliminarlogtodo(){
      $dato=DB::table('log')->truncate();
      dd("los registros fueron eliminados");
    }



}

