<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class dashboard_controlador extends Controller
{
    //

   public function __construct()
    {
        $this->middleware('auth');
    }
  public function consolidadoChart(){
      $year=date('Y');
      $anual=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-01-01\' AND \''.$year.'-01-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-01-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-01-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-01-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-01-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-01-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));
    	$enero=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-01-01\' AND \''.$year.'-01-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-01-01\' AND \''.$year.'-01-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-01-01\' AND \''.$year.'-01-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-01-01\' AND \''.$year.'-01-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-01-01\' AND \''.$year.'-01-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-01-01\' AND \''.$year.'-01-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

    	$febrero=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-02-01\' AND \''.$year.'-02-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-02-01\' AND \''.$year.'-02-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-02-01\' AND \''.$year.'-02-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-02-01\' AND \''.$year.'-02-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-02-01\' AND \''.$year.'-02-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-02-01\' AND \''.$year.'-02-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

    	$marzo=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-03-01\' AND \''.$year.'-03-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-03-01\' AND \''.$year.'-03-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-03-01\' AND \''.$year.'-03-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-03-01\' AND \''.$year.'-03-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-03-01\' AND \''.$year.'-03-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-03-01\' AND \''.$year.'-03-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

    	$abril=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-04-01\' AND \''.$year.'-04-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-04-01\' AND \''.$year.'-04-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-04-01\' AND \''.$year.'-04-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-04-01\' AND \''.$year.'-04-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-04-01\' AND \''.$year.'-04-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-04-01\' AND \''.$year.'-04-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $mayo=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-05-01\' AND \''.$year.'-05-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-05-01\' AND \''.$year.'-05-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-05-01\' AND \''.$year.'-05-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-05-01\' AND \''.$year.'-05-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-05-01\' AND \''.$year.'-05-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-05-01\' AND \''.$year.'-05-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $junio=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-06-01\' AND \''.$year.'-06-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-06-01\' AND \''.$year.'-06-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-06-01\' AND \''.$year.'-06-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-06-01\' AND \''.$year.'-06-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-06-01\' AND \''.$year.'-06-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-06-01\' AND \''.$year.'-06-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $julio=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-07-01\' AND \''.$year.'-07-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-07-01\' AND \''.$year.'-07-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-07-01\' AND \''.$year.'-07-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-07-01\' AND \''.$year.'-07-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-07-01\' AND \''.$year.'-07-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-07-01\' AND \''.$year.'-07-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $agosto=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-08-01\' AND \''.$year.'-08-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-08-01\' AND \''.$year.'-08-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-08-01\' AND \''.$year.'-08-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-08-01\' AND \''.$year.'-08-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-08-01\' AND \''.$year.'-08-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-08-01\' AND \''.$year.'-08-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $sept=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-09-01\' AND \''.$year.'-09-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-09-01\' AND \''.$year.'-09-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-09-01\' AND \''.$year.'-09-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-09-01\' AND \''.$year.'-09-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-09-01\' AND \''.$year.'-09-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-09-01\' AND \''.$year.'-09-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $oct=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-10-01\' AND \''.$year.'-10-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-10-01\' AND \''.$year.'-10-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-10-01\' AND \''.$year.'-10-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-10-01\' AND \''.$year.'-10-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-10-01\' AND \''.$year.'-10-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-10-01\' AND \''.$year.'-10-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $nov=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-11-01\' AND \''.$year.'-11-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-11-01\' AND \''.$year.'-11-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-11-01\' AND \''.$year.'-11-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-11-01\' AND \''.$year.'-11-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-11-01\' AND \''.$year.'-11-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-11-01\' AND \''.$year.'-11-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));

      $dic=DB::select( DB::raw('SELECT  SUM(CASE WHEN fecha_soat BETWEEN \''.$year.'-12-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as soat,
       SUM(CASE WHEN fecha_poliza_resp BETWEEN \''.$year.'-12-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as polizaresp,
       SUM(CASE WHEN fecha_tarjetaop BETWEEN \''.$year.'-12-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as tarjetaop,
       SUM(CASE WHEN fecha_todo_riesgo BETWEEN \''.$year.'-12-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as todo,
       SUM(CASE WHEN fecha_tecnomecanica BETWEEN \''.$year.'-12-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as tecno, 
       SUM(CASE WHEN fecha_preventiva BETWEEN \''.$year.'-12-01\' AND \''.$year.'-12-30\' THEN 1 ELSE 0 END) as preventiva FROM vehiculo '));
    	           
         return view('dashboard.dashboard', ['enero' => $enero,'febrero' => $febrero, 'marzo'=> $marzo,'abril'=>$abril,'mayo'=>$mayo,'junio'=>$junio,'julio'=>$julio,'agosto'=>$agosto,'sept'=>$sept,'oct'=>$oct,'nov'=>$nov,'dic'=>$dic,"year"=>$year,'anual'=>$anual]); 
         
  }
}
