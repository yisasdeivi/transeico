<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/inicio';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
   {
       $validator= Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
       if ($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
       if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->intended('inicio');
        }
        else{
         return back()->withErrors(['email'=>'no se encuentra este usuario o la contraseña es erronea']);
        }
    
           return back()->withErrors(['email'=>'error']);
        
    
   }

   public function logout(){
    Auth::logout();
    return redirect('/');
   }
}
