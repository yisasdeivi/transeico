<?php

namespace App\Http\Controllers;
use App\dato;
use Illuminate\Http\Request;
use Storage;
use Excel;
use DB;
use DateTime;
class datos_controlador extends Controller
{
        public  $nombre="jesus";

   public function __construct()
    {
        $this->middleware('auth');
    }
 
     /** metodo cargardatos recibe el archivo Excel
     ** que se encarga de importar los datos a la base de datos
     **
     **
     */   
    public function cargardatos(Request $request)
    {
      
       $archivo = $request->file('archivo');
       $nombre_original=$archivo->getClientOriginalName();
	   $extension=$archivo->getClientOriginalExtension();
       $r1=Storage::disk('local')->put($nombre_original,  \File::get($archivo) );
       $ruta  = storage_path('app')."/".$nombre_original;
       
       
      if($r1){
       	   
           $columna= Excel::selectSheetsByIndex(0)->load($ruta, function($hoja) {
           	  $hoja->toArray();
           	  $hoja->noHeading(); })->get();
		      $cont=1;
		      $this->eliminarTodos();//elimina todos los registros		           
            foreach ($columna as $key => $filas)
             {
             	  if($cont>1)
                {                   
                	if(!$this->validarNull($filas))
                  {
                    $fila=$this->filtrocampos($filas); 
                   if (DB::table('persona')->where('cedula', '=', $this->separarNumero($fila[28]))->count() === 0) {          	                	  
                	  $persona=['cedula'=>$this->separarNumero($fila[28]),'nombre'=>$fila[27],'password'=>$this->separarNumero($fila[28]),'email'=>$fila[29]];
                      DB::table('persona')->insert($persona);
                      
                    }else{
                      
                    }
                    if (DB::table('propietario')->where('cedula', '=', $this->separarNumero($fila[28]))->count() === 0){
                        $prop=['cedula'=> $this->separarNumero($fila[28])];
                        DB::table('propietario')->insert($prop);
                      }
            	     } 

            	   }	
            	     
            	$cont=$cont +1;
            }

            foreach ($columna as $key => $filas)
             {
              $formato ='Y-m-d H:i:s';
                if($cont>1)
                {                   
                  if(!$this->validarNull($filas))
                  {
                   $fila=$this->filtrocampos($filas); 
                   $dato[]= ['placa' => $fila[1], 'marca' => $fila[5],'color' => $fila[9], 'modelo' => $fila[7],'tipo_vehi'=>$fila[11], 'cedula_propietario' => $this->separarNumero($fila[28]),'combustible' => $fila[22], 'modelo' => $fila[7],'lic_transito' => $fila[34], 'soat' => $fila[39],'fecha_soat' => $fila[42], 'compania_soat' => $fila[40],'poliza_resp' => $fila[51],'poliza_respex'=>$fila[55], 'fecha_poliza_resp' => $fila[53],'tarjetaop' => $fila[44], 'fecha_tarjetaop' => $fila[47],'todo_riesgo'=>$fila[60], 'fecha_todo_riesgo'=>$fila[61],'tecnomecanica' => $fila[63],'fecha_tecnomecanica' => $fila[66],'fecha_preventiva'=>$fila[69], 'nombre_conductor' => $fila[73]];
                 }
              $cont=$cont +1;
            }
          }
           //dd($dato);
          if(!empty($dato)){
			    DB::table('vehiculo')->insert($dato);
		      dd('Registros insertados con Exito!.');
			   }
                    

       

    }
    Storage::disk('s3')->delete($ruta);

      
	}
       /**Method privado 
       ** parametro> recibe fila del archivo de escel
       ** verifica si hay valores nulos y vacios, si excede los valores
       **  nulos y vacio no se toman
       **  retorna true, false
       **/
	   private function validarNull($fila)
	   {
        $cont=0;  
        $aux=0;     	
       	while ($aux<= 70) {                   	
       		if(is_null($fila[$aux])||empty($fila[$aux])){
              $cont=$cont+1;

       		}
       	$aux=$aux+1;
    
       	}
       //	print_r('numero:'.$cont.'\n');
       	
       	 if($cont>20)
       		 return true;        
       return false;
       	
       }
    /**Method privado que permite separa numero de una cadena
       ** parametro> recibe fila del archivo de escel
       ** separa las cadenas y incorpora los numeros en una cadena
       ** 
       **/
       private function separarNumero($cadena){
       	$numero="";
        if(!is_numeric($cadena)){
       	 for( $index = 0; $index < strlen($cadena); $index++ )
       	     {
             if( is_numeric($cadena[$index]) )
                     {
                    $numero .= $cadena[$index];
                      }
              }  
          return $numero;
        }
        return $cadena;
       }
       //mretodo que filtra los campos de el archivo excel que contiene caracteres no deseado para los tipos de datos de la BD
       private function filtrocampos($fila){ 
        $aux=0; 
        $fil=$fila;   	
       	while ($aux <= 70) {   
       	 if((strcmp($fil[$aux],'N/A')===0)||(strcmp($fil[$aux],'N/T'))===0||(strcmp($fil[$aux],'-'))===0){
       	 	$fil[$aux]=null;
             echo '<script>';
             echo 'console.log('. json_encode( $fil[$aux]) .')';
             echo '</script>'; 
       	 	
       	 }       	
        $aux=$aux+1;
         }
       	return $fil;
       }
       //se trunquea toda la tabla eliminando registros y indexando los incremento en 0 antes de insertar
      private function eliminarTodos()
      {
      	
        
        
        $table=DB::table('vehiculo')->delete(); 
        if(count(DB::table('vehiculo')->get())<1){
         $table=DB::table('propietario')->delete();     
        }
        $table=DB::table('persona')->delete();   

      }     

     function log( $data ){
  echo '<script>';
  echo 'console.log('. json_encode( $data ) .')';
  echo '</script>';
}
  }

