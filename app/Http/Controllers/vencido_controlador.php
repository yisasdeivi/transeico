<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class vencido_controlador extends Controller
{
    //

public function __construct()
    {
        $this->middleware('auth');
    }
 public function index(){
 	return view ('listado.docvencido');
 }
 public function documentoVencido(Request $req){
     $in=$req->input('in');
     $end=$req->input('end');
 	 $db=DB::select( DB::raw('select * from (select placa,marca,modelo,cedula_propietario,(CASE WHEN fecha_soat BETWEEN \''.$in.'\'AND \''.$end.'\' THEN concat(\'vence el: \',fecha_soat) END)as soat ,
       (CASE WHEN fecha_poliza_resp BETWEEN \''.$in.'\'AND \''.$end.'\' THEN concat(\'vence el: \',fecha_poliza_resp)  END) as polizaresp ,
       (CASE WHEN fecha_tarjetaop BETWEEN \''.$in.'\'AND \''.$end.'\' THEN concat(\'vence el: \',fecha_tarjetaop) END) as tarjetaop ,
       (CASE WHEN fecha_todo_riesgo BETWEEN \''.$in.'\'AND \''.$end.'\' THEN concat(\'vence el: \',fecha_todo_riesgo)  END) as todo ,
       (CASE WHEN fecha_tecnomecanica BETWEEN \''.$in.'\'AND \''.$end.'\' THEN concat(\'vence el: \',fecha_tecnomecanica)  END) as tecno , 
       (CASE WHEN fecha_preventiva BETWEEN \''.$in.'\'AND \''.$end.'\' THEN concat(\'vence el: \',fecha_preventiva) END)  as preventiva from vehiculo)as consulta inner join persona p on cedula_propietario = p.cedula where not (soat is null and polizaresp is null and tarjetaop is null and todo is null and tecno is null and preventiva is null)
      '));

 	return view ('listado.docvencidos',['db'=>$db]);
 } 
}
