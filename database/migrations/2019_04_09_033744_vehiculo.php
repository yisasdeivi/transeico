<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vehiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculo',function(Blueprint $table){
            $table->String('placa',100);  
            $table->String('marca',150)->nullable();
            $table->String('color',100)->nullable();            
            $table->String('modelo',150)->nullable();
            $table->String('tipo_vehi',100)->nullable();
            $table->String('cedula_propietario',100);           
            $table->String('combustible',50)->nullable();
            $table->String('lic_transito',250)->nullable();
            $table->String('soat',250)->nullable();
            $table->date('fecha_soat')->nullable();
            $table->String('compania_soat')->nullable();
            $table->String('poliza_resp',250)->nullable();//poliza de responsabilidad civil
            $table->String('poliza_respex',250)->nullable();//poliza de responsabilidad extraconstractual
            $table->date('fecha_poliza_resp')->nullable();//la misma fecha
            $table->String('tarjetaop',250)->nullable();//            /
            $table->date('fecha_tarjetaop')->nullable();//
            $table->String('todo_riesgo',250)->nullable();//
            $table->date('fecha_todo_riesgo')->nullable();//
            $table->String('tecnomecanica',250)->nullable();
            $table->date('fecha_tecnomecanica')->nullable();
            $table->date('fecha_preventiva')->nullable();//preventiva
            $table->String('nombre_conductor',250)->nullable();
            $table->foreign('cedula_propietario')->references('cedula')->on('propietario')->onDelete('cascade');;
            $table->primary('placa');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehiculo');
    }
}
