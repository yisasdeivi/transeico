
       <div class="box box-primary">

<div class="box-header">
                  <h3 class="box-title">Consolidado</h3>
                </div>
<div class="box-body">
<div  id="piechart_3d" style="width: 500px; height: 400px;text-align: center; "></div> <div id="chart_div">
</div>	
</div>

    
 <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Items', 'Vencimiento'],
          ['Soat',   <?php echo $anual[0]->soat ?>],
          ['RCC/RCE',   <?php echo $anual[0]->polizaresp ?>],
          ['TOperacion',  <?php echo $anual[0]->tarjetaop?>],
          ['TodoRiesgo',   <?php echo $anual[0]->todo ?>],
          ['TecnoMecanica',<?php echo $anual[0]->tecno ?>],
          ['Preventiva',<?php echo $anual[0]->preventiva ?>],
        ]);

        var options = {
          title: 'Vencimiento de documento por año',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['<?php echo $year; ?>', 'Soat', 'RCC/RCE', 'TOperacion','TodoRiesgo','TecnoMecan..','Preventiva'],
  
       <?php   echo "['Enero',   ".$enero[0]->soat.", ".$enero[0]->polizaresp.",".$enero[0]->tarjetaop.",".$enero[0]->todo.",".$enero[0]->tecno.",".$enero[0]->preventiva."],
          ['Febrero', ".$febrero[0]->soat.", ".$febrero[0]->polizaresp.",".$febrero[0]->tarjetaop.",".$febrero[0]->todo.",".$febrero[0]->tecno.",".$febrero[0]->preventiva."],
          ['Marzo', ".$marzo[0]->soat.", ".$marzo[0]->polizaresp.",".$marzo[0]->tarjetaop.",".$marzo[0]->todo.",".$marzo[0]->tecno.",".$marzo[0]->preventiva."],
          ['Abril',".$abril[0]->soat.", ".$abril[0]->polizaresp.",".$abril[0]->tarjetaop.",".$abril[0]->todo.",".$abril[0]->tecno.",".$abril[0]->preventiva."],
          ['Mayo',".$mayo[0]->soat.", ".$mayo[0]->polizaresp.",".$mayo[0]->tarjetaop.",".$mayo[0]->todo.",".$mayo[0]->tecno.",".$mayo[0]->preventiva."],
          ['Junio',".$junio[0]->soat.", ".$junio[0]->polizaresp.",".$junio[0]->tarjetaop.",".$junio[0]->todo.",".$junio[0]->tecno.",".$junio[0]->preventiva."],
          ['Julio',".$julio[0]->soat.", ".$julio[0]->polizaresp.",".$julio[0]->tarjetaop.",".$julio[0]->todo.",".$julio[0]->tecno.",".$julio[0]->preventiva."], 
          ['Agosto',".$agosto[0]->soat.", ".$agosto[0]->polizaresp.",".$agosto[0]->tarjetaop.",".$agosto[0]->todo.",".$agosto[0]->tecno.",".$agosto[0]->preventiva."],
          ['Septiembre',".$sept[0]->soat.", ".$sept[0]->polizaresp.",".$sept[0]->tarjetaop.",".$sept[0]->todo.",".$sept[0]->tecno.",".$sept[0]->preventiva."],
           ['Octubre',".$oct[0]->soat.", ".$oct[0]->polizaresp.",".$oct[0]->tarjetaop.",".$oct[0]->todo.",".$oct[0]->tecno.",".$oct[0]->preventiva."],
           ['Noviembre',".$nov[0]->soat.", ".$nov[0]->polizaresp.",".$nov[0]->tarjetaop.",".$nov[0]->todo.",".$nov[0]->tecno.",".$nov[0]->preventiva."],
            ['Diciembre',".$dic[0]->soat.", ".$dic[0]->polizaresp.",".$dic[0]->tarjetaop.",".$dic[0]->todo.",".$dic[0]->tecno.",".$dic[0]->preventiva."],"
          ;?>
         
        ]);

        var options = {
          chart: {
            title: 'Consolidado anual',
            subtitle: 'Vencimiento de los documentos por meses: del año  <?php echo $year ?>',
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#1b9e77', '#d95f02', '#7570b3','#00FFFF','#800000','#808080','#000000']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

          if (e.target.tagName === 'BUTTON') {
            options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
            chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
      }
      </script>