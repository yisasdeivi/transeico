
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<div class="box box-primary">

<div class="box-header">
                  <h3 class="box-title">Datos Vehiculos</h3>
                </div>

<div class="box-body"> 

<form class="form-inline form_venci" id="form_venci">
  <div class="form-group mx-sm-3 mb-2">
    <div class="input-group date" data-date-format="yyyy.mm.dd">

            <input  type="text" class="form-control" placeholder="Fecha Inicial" id="in">
            <div class="input-group-addon" >
              <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>

     <div class="input-group date" data-date-format="yyyy.mm.dd">

            <input  type="text" class="form-control" placeholder="Fecha Final" id="end">
            <div class="input-group-addon" >
              <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>  

  <button type="submit" class="btn btn-primary mb-2">Buscar!</button>
</form> 

</div>
<div class="box-primary" id='documentosVen'></div>


<script type="text/javascript">

    $('.date').datepicker({  

       format: 'yyyy-mm-dd'

     });  

</script>  



</body>

</html>