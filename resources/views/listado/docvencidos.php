<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<div class="box box-primary">

<div class="box-header">
                  <h3 class="box-title">Datos Vehiculos</h3>
                </div>

<div class="box-body"> 

<form class="form-inline form_venci" id="form_venci">
  <div class="form-group mx-sm-3 mb-2">
    <div class="input-group date" data-date-format="yyyy.mm.dd">

            <input  type="text" class="form-control" placeholder="Fecha Inicial" id="in">
            <div class="input-group-addon" >
              <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>

     <div class="input-group date" data-date-format="yyyy.mm.dd">

            <input  type="text" class="form-control" placeholder="Fecha Final" id="end">
            <div class="input-group-addon" >
              <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>  

  <button type="submit" class="btn btn-primary mb-2">Buscar!</button>
</form> 

</div>
<div class="box-primary" id='documentosVen'>
<?php  
if( count($db) >0){
?>
<div class="scrollme" style="overflow:scroll;">
 <table class="table " id="table">
       
        <thead>
            <tr>
                <th>Placa</th>
                <th>Modelo</th>
                <th>Marca</th>
                <th>Propietario</th>
                <th>Nit/Cedula</th>
                <th>Soat</th>
                <th>RCC/RCE</th>
                <th>Tarjeta Operacion</th>
                <th>Todo Riesgo</th>
                <th>Tecnomecanica</th>
                <th>Preventiva</th>
                            
            </tr>
        </thead>
 
       
<tbody>


<?php 

   foreach($db as $da){  
?>

<tr role="row" class="odd">
    <td>
    </i>&nbsp;&nbsp;<?= $da->placa;  ?></a></td>
    <td class="red"><?= $da->modelo ;  ?></td>
    <td><?= $da->marca;  ?></td>
    <td><?= $da->nombre ;  ?></td>
    <td><?= $da->cedula;  ?></td>
    <td style="color:#FF0000";><?= $da->soat ;  ?></td>
    <td style="color:#FF0000";><?= $da->polizaresp;  ?></td>
    <td style="color:#FF0000";><?= $da->tarjetaop;  ?></td>
    <td style="color:#FF0000";><?= $da->todo;  ?></td>
    <td style="color:#FF0000";><?= $da->tecno;  ?></td>
    <td style="color:#FF0000";><?= $da->preventiva;  ?></td>
  
       
       

 </tr>   
       

 </tr>   

<?php        
}

?>
  

    </table>
</div>
</div>

<?php        
}

?>
<script type="text/javascript">

    $('.date').datepicker({  

       format: 'yyyy-mm-dd'

     });  

</script>  



</body>

</html>