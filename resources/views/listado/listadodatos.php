<div class="box box-primary">

<div class="box-header">
                  <h3 class="box-title">Datos Vehiculos</h3>
                </div>

<div class="box-body"> 

<form class="form-inline form_buscar" id="form_buscar">
  <div class="form-group mx-sm-3 mb-2">
    <label for="inputPassword2" class="sr-only">Buscar</label>
    <input type="text" class="form-control" name="buscar" id="buscar" placeholder="ingresar">
  </div>
  <button type="submit" class="btn btn-primary mb-2">Buscar!</button>
</form>             
<?php 

if( count($dato) >0){
?>
<div class="scrollme" style="overflow:scroll;">
 <table class="table " id="table">
       
        <thead>
            <tr>
                <th style="width:10px">Placa </th>
                <th>Marca </th>
                <th>Color</th>
                <th>Modelo</th>
                <th>Tipo Vehiculo</th>
                <th>Nombre Propietario</th>
                <th>Cedula Propietario</th>
                <th>Nombre de Conductor</th>
                <th>Tipo de combustible</th>
                <th>Licencia de Transito</th>
                <th>Numero de SOAT</th>
                <th>Fecha de Venc SOAT</th>
                <th>Compañia Aseguradora de SOAT</th>
                <th>Poliza RCC.</th>
                <th>Fecha Poliza RCC</th>
                <th>Poliza RCE</th>
                <th>Fecha Poliza RCE</th>
                <th>Tarjeta de Operacion No.</th>
                <th>Fecha venci Tarjeta de Operacion</th>
                <th>Tecnomecanica</th>
                <th>Fecha Vencimiento Tecnomecanica</th>
                <th>Todo Riesgo</th>
                <th>Fecha Vencimiento Todo Riesgo</th>
                <th>Fecha Vencimiento Preventiva</th>

              
            </tr>
        </thead>
 
       
<tbody>


<?php 

   foreach($dato as $da){  
?>

 <tr role="row" class="odd">
    <?php 
      if(strcasecmp ($da->tipo_vehi,'CAMIONETA')===0){?><td class="mailbox-messages mailbox-name" ><i class="fas fa-truck-pickup">
       <?php }if(strcasecmp ($da->tipo_vehi,'CAMPERO')==0){?><td class="mailbox-messages mailbox-name" ><i class="fas fa-truck-monster">
      <?php }if(strcasecmp ($da->tipo_vehi,'MICROBUS')===0){?> <td class="mailbox-messages mailbox-name" ><i class="fas fa-shuttle-van"> <?php }?>
    </i>&nbsp;&nbsp;<?= $da->placa;  ?></a></td>
    <td><?= $da->marca;  ?></td>
    <td><?= $da->color;  ?></td>
    <td><?= $da->modelo;  ?></td>
    <td><?= $da->tipo_vehi;?></td>
    <td><?= $da->nombre;  ?></td>
    <td><?= $da->cedula_propietario;  ?></td>
     <td><?= $da->nombre_conductor;  ?></td>
     <td><?= $da->combustible;  ?></td>
      <td><?= $da->lic_transito;  ?></td>
      <td><?= $da->soat;  ?></td>
      <td><?= $da->fecha_soat;  ?></td>
      <td><?= $da->compania_soat;  ?></td>
      <td><?= $da->poliza_resp;  ?></td>
      <td><?= $da->fecha_poliza_resp;  ?></td>
      <td><?= $da->poliza_respex;  ?></td>
      <td><?= $da->fecha_poliza_resp;  ?></td>
      <td><?= $da->tarjetaop;  ?></td>
      <td><?= $da->fecha_tarjetaop;  ?></td>
      <td><?= $da->tecnomecanica;  ?></td>
      <td><?= $da->fecha_tecnomecanica;  ?></td>
      <td><?= $da->todo_riesgo;  ?></td>
     <td><?= $da->fecha_todo_riesgo;  ?></td>
     <td><?= $da->fecha_preventiva;  ?></td>


       

 </tr>   

<?php        
}
?>
  

    </table>
</div>


    <?php


echo str_replace('/?', '?', $dato->render() )  ;

}
else{

?>

<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun usuario...</label>  </div> 

<?php
}

?>
</div>



